#!/usr/bin/env bash
'''
function record_result() {
  outputfile=${1}
  context=${2}
  exec 200>$outputfile

  flock -n 200 || {
      echo "Other processes are recording results, waiting..."
      flock -x 200
  }

  echo "${context}" >> ${outputfile}

  flock -u 200
  exec 200>&-
}
'''
function record_result() {
  local outputfile=${1}
  local context=${2}
  echo "${context}" >> ${outputfile}
}
record_result "$@"
