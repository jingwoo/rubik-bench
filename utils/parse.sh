#!/usr/bin/env bash

function get_item_value_from_json() {
  jsonFile=${1}
  key=${2}
  cat ${jsonFile} | jq -c ${key}
}

function get_array_value_from_json() {
  jsonFile=${1}
  key=${2}
  cat ${jsonFile} | jq -c ${key}|length
}


function get_value_from_yaml() {
  yamlFile=${1}
  key=${2}
  cat ${yamlFile} | shyaml values ${key}
}
