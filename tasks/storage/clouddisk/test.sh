#!/usr/bin/env bash

function execute_test() {
  local result_file=${1}
  local lock_file=${2}
  for i in $(seq 1 10) ;do
      sleep .$RANDOM
      echo "worker ($i): $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $((RANDOM%80 + 1)) | head -n 1)"
  done

  # name(category-app): result
  IOPS=$((1 + RANDOM % 10000))
  Throughput=$((1 + RANDOM % 1000))
  Latency=$((1 + RANDOM % 1000))
  result="storage-clouddisk: IOPS-"${IOPS}";Throughput-"${Throughput}";Latency-"${Latency}""
  flock -x ${lock_file} -c "sh ./utils/record_results.sh \"${result_file}\" \"${result}\""
}

execute_test "$@"