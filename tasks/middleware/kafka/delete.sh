#!/usr/bin/env bash

function delete() {
  for i in $(seq 1 10) ;do
      sleep .$RANDOM
      echo "worker ($i): $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $((RANDOM%80 + 1)) | head -n 1)"
  done
}

delete
