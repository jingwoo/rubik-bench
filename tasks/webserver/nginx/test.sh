#!/usr/bin/env bash

function execute_test() {
  local result_file=${1}
  local lock_file=${2}
  for i in $(seq 1 10) ;do
      sleep .$RANDOM
      echo "worker ($i): $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $((RANDOM%80 + 1)) | head -n 1)"
  done

  # name(category-app): result
  spend_time=1250
  fKRQS=$((1 + RANDOM % 10000))
  oMRQS=$((1 + RANDOM % 5000))
  result="webserver-nginx: 4KRQS-${fKRQS};1MRQS-${oMRQS}"
  flock -x ${lock_file} -c "sh ./utils/record_results.sh \"${result_file}\" \"${result}\""
}

execute_test "$@"