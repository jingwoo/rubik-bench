FROM hub.oepkgs.net/openeuler/openeuler:20.03-lts-sp3

MAINTAINER zhangxiaoyu <zhangxiaoyu58@huawei.com>

ENV APP /root/rubik-bench

RUN mkdir $APP
WORKDIR $APP

RUN yum install -y util-linux \
		hostname \
		bc \
		iotop \
		sysstat \
		intel-cmt-cat \
		iperf3 \
		docker

# COPY stress-ng static binary
COPY stress-ng /usr/bin
COPY framework ./framework

WORKDIR ${APP}/framework

ENTRYPOINT ["/usr/bin/bash", "run_collect.sh"]
