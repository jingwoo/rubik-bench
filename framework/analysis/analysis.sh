#!/usr/bin/env bash

declare -ar colors=("red" "blue" "green" "magenta" "yellow" "black" "cyan")

function analysis() {
  native_result=${1}
  custom_result=${2}

  declare -A results
  while read line; do
    local application=$(echo ${line} | awk -F": " '{print $1}')
    local scores=$(echo ${line} | awk -F": " '{print $NF}')
    results[${application}]=${scores}
  done < ${native_result}

  for key in ${!results[@]}; do
    local score=${results[$key]}
    local elements=(${score//;/ })
    local len=${#elements[@]}

    local custom_score=$(cat ${custom_result} | grep ${key} | awk -F": " '{print $NF}')
    local custom_elements=(${custom_score//;/ })

    declare -a lines=()
    for ((i=0;i<${#elements[@]};i++)); do
      local datas=""
      datas=$datas$(echo ${elements[${i}]} | awk -F"-" '{print $1}'),
      datas=$datas$(echo ${elements[${i}]} | awk -F"-" '{print $NF}'),
      datas=$datas$(echo ${custom_elements[${i}]} | awk -F"-" '{print $NF}')
      lines[${#lines[@]}]=${datas}
    done
    printf -v format_var "%s\n" "${lines[@]}"
    echo -e "@ native,custom
${format_var}" | termgraph --color {red,blue} --title ${key}
    echo "--------------------------------------------------------------------------------------"
  done
}

analysis "$@"