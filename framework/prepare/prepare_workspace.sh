#!/usr/bin/env bash

function prepare_workspace() {
  local dir=${1}
  local result=${2}
  mkdir -p ${dir}
  touch ${dir}/${result}

  rm -rf ./logs > /dev/null
  mkdir logs
}

prepare_workspace ${1} ${2}
