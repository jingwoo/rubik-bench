import subprocess


class cgroupHelper:
    @classmethod
    def supportCgv2(cls):
        proc = subprocess.Popen(
            "mount | grep cgroup2", stdout=subprocess.PIPE, shell=True
        )
        out, _ = proc.communicate()
        return len(out) > 0

    @classmethod
    def get_path(cls, cgtype, **kwargs):
        v, driver, ctrID, cgroupPrefix = (
            kwargs["v"],
            kwargs["driver"],
            kwargs["ctrID"],
            kwargs["cgroupPrefix"],
        )
        if v == "v2" and driver == "cgroupfs":
            return "/".join([cgroupPrefix, "docker", ctrID])

        if v == "v2" and driver == "systemd":
            return "/".join(
                [cgroupPrefix, "system.slice", "docker-" + ctrID + ".scope"]
            )

        if v == "v1" and driver == "cgroupfs":
            return "/".join([cgroupPrefix, cgtype, "docker", ctrID])

        if v == "v1" and driver == "systemd":
            return "/".join(
                [
                    cgroupPrefix,
                    cgtype,
                    "system.slice",
                    "docker-" + ctrID + ".scope",
                ]
            )

    @classmethod
    def get_cpu_path(cls, **kwargs):
        return cgroupHelper.get_path("cpu", **kwargs)

    @classmethod
    def get_memory_path(cls, **kwargs):
        return cgroupHelper.get_path("memory", **kwargs)

    @classmethod
    def get_blkio_path(cls, **kwargs):
        return cgroupHelper.get_path("blkio", **kwargs)

    @classmethod
    def get_network_path(cls, **kwargs):
        return cgroupHelper.get_path("network", **kwargs)


class supHelper:
    @classmethod
    def getNodeSupports(cls):
        return [
            "pressure",
            "rdt",
            "cpu",
            "network",
            "io",
            "memory",
            "process",
        ]

    @classmethod
    def getNodeClasses(cls):
        return {
            "cpu": "NodeCpuCollect",
            "network": "NodeNetCollect",
            "io": "NodeIoCollect",
            "memory": "NodeMemCollect",
            "process": "NodeProcCollect",
            "pressure": "PressureCollect",
            "rdt": "RdtCollect",
        }

    @classmethod
    def getCtrSupports(cls):
        return ["pressure", "cpu", "rdt", "io", "memory"]

    @classmethod
    def getCtrClasses(cls):
        return {
            "pressure": "PressureCollect",
            "cpu": "CtrCpuCollect",
            "rdt": "RdtCollect",
            "io": "CtrIoCollect",
            "memory": "CtrMemCollect",
        }
