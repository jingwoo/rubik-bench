class BccProcess(object):
    def __init__(self):
        pass

    @staticmethod
    def process_log2(out):
        """process bpf log2 histogram

        range: 2^n usecs for n in (0, 1, ..., 63)
        count: counts in 2^n range
        histogram: (counts in 2^n range) / (total count)

        Only return histogram bcoz counts is meaningless if timeout changes
        """
        # if -T or -pid is added in bcc cmd, beginLine should also increased
        beginLine = 4
        lines = out.decode("utf-8").split("\n")[beginLine:-1]
        counts = [int(line.split()[4]) for line in lines]
        totalCount = sum(counts)
        hist = [format(count / totalCount, ".3f") for count in counts]
        return hist


class PerfProcess(object):
    @staticmethod
    def process_perf_stat(out, delimiter=","):
        out = out.decode("utf-8").split("\n")
        row = [line.split(delimiter)[0] for line in out]
        return row

    @staticmethod
    def process_perf_trace(out):
        raise NotImplementedError(self.__class__.__name__)
