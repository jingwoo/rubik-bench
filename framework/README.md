# 介绍

rubik bench项目主要目的是帮助用户进行数据采集和压力测试，从而帮助用户了解业务的资源敏感性等信息。当前项目主要是通过shell实现，数据采集需要借助`pqos`、`iotop`等命令实现，压力测试则是通过`stress-ng`以及`iperf3`等压测工具实现。

# 使用说明

### 帮助信息

```bash
$ bash run_collect.sh --help
Usage: run_collect.sh [options]
Run collection and stress test
Options:
    -c, --collect                     Enable machine and container info collection
    -t, --timeout                     Collection timeout time

    -s, --stress                      Enable stress test
        --stress-collect              Enable stress info collection
        --stress-cpu                  Enable cpu stress
        --stress-memory               Enable memory stress
        --stress-cache                Enable cache stress
        --stress-cpu-cache-mixed      Enable cpu and cache mixed stress

    -w, --stress-wait                 Run stress after wait time
        --disk-location               Enable and run disk io stress at location
        --network-ip                  Enable and run network stress connect to server IP
        --network-port                Network stress connect to server port(default 5201)

    -h, --help                        Help information
```

### 基本使用方法

```bash
$ bash run_collect.sh -c -t 1h -s --stress-collect --stress-cpu --stress-memory --stress-cache --stress-cpu-cache-mixed -w 5m --disk-location ./ --network-ip 192.168.0.1 --network-port 5201
```

| options | 说明 |
| - | - |
| -c, --collect | 开启mechine及container数据收集功能 |
| -t, --timeout | 需要指定时间，collect及stress-collect超时时间 |
| -s, --stress | 开启压力测试 |
| --stress-collect | 开启stress压力测试的数据收集功能 |
| --stress-cpu |开启cpu压力测试，压力从1逐步爬升至总核数的70% |
| --stress-memory | 开启memory压力测试，压力从available memory的5%逐步爬升至70% |
| --stress-cache | 开启cache压力测试，分别测试icache/dcache以及L2/L3 cache，压力从1逐步爬升至总核数 |
| --stress-cpu-cache-mixed | 开启cpu与cache的混合测试，同时注入cpu、icache、dcache、L2cache、L3cache压力 |
| -w, --stress-wait | 需要指定时间，stress压力测试前等待时间，业务可能需要“暖身”一段时间后才能稳定 |
| --disk-location | 需要指定路径，开启磁盘压力测试，测试指定路径处的压力 |
| --network-ip | 需要指定IP地址，开启网络压力测试，需要对端使用iperf3 -s命令开启监听，压力从50M/s逐步爬升至1000M/s |
| --network-port | 需要指定端口，网络压力测试对端的监听端口，不指定默认为5201 |

# 测试结果说明

数据收集的结果存放在/tmp/collect目录下。此外/tmp目录下还保留了之前执行收集的结果，目录形式为/tmp/collect_XXX，每次执行run_collect.sh时，会将/tmp/collect目录重命名为/tmp/collect_XXX，然后重新创建/tmp/collect目录并在其中存放收集的数据信息。

collect目录下面主要包含以下几个文件
```bash
$ ls /tmp/collect

26c1fab9611b4e9f41d82b7f8c5939513bfb9a67d0334fadcb6dd9b037875679-container.data  centos1-node-disk-detail.data
2bd229c4157751dd4892d089ef4ac7c965b8015e957932f46454abb40d9dcbe7-container.data  f32fd2b2ee98cf7a31439e891ab6ebe95ab9f0c86ef5af6d2bd64ce0aaf7aa64-container.data
698792df33a205a10573ee1b29466b0063d86beaae82d0d799f21424a87b9653-container.data  stress_info
c1690c80446ae5a5b8149a5739f2368f364b2ffa8fa3361ce59adf8d3d03a1c0-container.data  stress_usage
centos1-node.data
```

其中

XXX-container.data文件，主要保存了收集的容器信息。

包括容器的基本信息
| Field            | Type       | Label | Comment                                     |
| -                | -          | -     | -                                           |
| container_id     | string     |       | uid of container                            |
| machine_id       | string     |       | uid of container host                       |
| image            | string     |       | container image                             |
| container_unit   | string     |       | container unit name                         |
| pod_name         | string     |       | name of pod container belongs to            |
| cpu_request      | bigint     |       | cpu shares request, 1000 means 1 core       |
| cpu_limit        | bigint     |       | 1000 means 1 core                           |
| memory_limit     | bigint     |       | in bytes                                    |
| storage          | string     |       | mounts of container                         |

以及容器的资源使用信息

| Field            | Type       | Label | Comment                                            |
| -                | -          | -     | -                                                  |
| time_stamp       | double     |       | time stamp, in second                              |
| cpu_usage        | bigint     |       | cpu usage, 1000 means 1 core                       |
| memory_usage     | bigint     |       | in bytes                                           |
| cpi              | double     |       | cycles per instruction(options)                    |
| mkpi             | bigint     |       | cache miss per second(options)                     |
| llc              | bigint     |       | llc occupancy, in KB(options)                      |
| lmb              | double     |       | local memory bandwidth, in MB/s(options)           |
| rmb              | double     |       | remote memory bandwidth, in MB/s(options)          |
| net_in           | double     |       | coming network traffic, bps                        |
| net_out          | double     |       | out going network traffic, bps                     |


```bash
$ cat /tmp/collect/26c1fab9611b4e9f41d82b7f8c5939513bfb9a67d0334fadcb6dd9b037875679-container.data

Container Metadata:
26c1fab9611b4e9f41d82b7f8c5939513bfb9a67d0334fadcb6dd9b037875679
centos1-aa027e8b689a4e97871c1e3fb707288b
sha256:5d3d5ddc8605ded8f69d76ee488072c7d02c32a8e4e8b34640a884c6eb939c0a
calico-kube-controllers
calico-kube-controllers-78d6f96c7b-97rt4
2
0
0
/dev/mapper/centos-root tmpfs

DATE    CPU USAGE       MEM USAGE       CPI     MKPI    LLC     LMB     RMB     NET I/O DISK I/O
2022-03-16 10:54:04     0       85626880        -1      -1      -1      -1      -1      eth0:0,0        8:0:0,0;253:0:0,0
2022-03-16 10:54:21     0       85626880        -1      -1      -1      -1      -1      eth0:1480,443   8:0:0,0;253:0:0,0
2022-03-16 10:54:38     1       85635072        -1      -1      -1      -1      -1      eth0:0,0        8:0:0,0;253:0:0,0
2022-03-16 10:54:55     0       85635072        -1      -1      -1      -1      -1      eth0:0,0        8:0:0,0;253:0:0,0
```
> 注：由于MKPI、LLC、LMB、RMB指标需要通过pqos工具辅助采集，且部分机器上不支持这些指标的采集，在无法采集到的机器上值为-1。

XXX-node.data，保存收集的节点相关信息

包括节点的基本信息
| Field            | Type       | Label | Comment                                     |
| -                | -          | -     | -                                           |
| machine_id       | string     |       | uid of machine                              |
| architecture     | string     |       | architecture of machine                     |
| model            | string     |       | cpu Model name                              |
| cpu_num          | string     |       | number of cpu on a machine                  |
| mem_size         | string     |       | memory size                                 |
| swap_size        | string     |       | memory size                                 |
| l1cache          | string     |       | L1 cache                                    |
| l2cache          | string     |       | L2 cache                                    |
| l3cahce          | string     |       | L3 cache                                    |

以及节点的资源使用信息
| Field            | Type       | Label | Comment                                            |
| -                | -          | -     | -                                                  |
| time_stamp       | double     |       | time stamp, in second                              |
| cpu_util_percent | bigint     |       | [0, 100]                                           |
| mem_util_percent | bigint     |       | [0, 100]                                           |
| cpi              | double     |       | cycles per instruction(options)                    |
| mkpi             | bigint     |       | cache miss per second(options)                     |
| llc              | bigint     |       | llc occupancy, in KB(options)                      |
| lmb              | double     |       | local memory bandwidth(options)                    |
| rmb              | double     |       | remote memory bandwidth(options)                   |
| net_in           | map bigint |       | coming network traffic, bps                        |
| net_out          | map bigint |       | out going network traffic, bps                     |
| disk_io          | string     |       | disk io (read/write)                               |

```bash
$ cat /tmp/collect/centos1-node.data

Machine Metadata:
machine ID: centos1-aa027e8b689a4e97871c1e3fb707288b
Linux centos1 3.10.0-957.el7.x86_64 #1 SMP Thu Nov 8 23:39:32 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
model name      : Intel(R) Xeon(R) CPU E5-2620 v3 @ 2.40GHz
On-line CPU(s) list:   0-11
MemTotal:        7815656 kB
SwapTotal:             0 kB
L1d cache:             32K
L1i cache:             32K
L2 cache:              4096K

DATE    CPU USAGE PERCENT       MEM USAGE PERCENT       CPI     MKPI    LLC     LMB     RMB     NET     DISK I/O
2022-03-16 10:59:42     13      21      -1      -1      -1      -1      -1      ens3:1191,1619  0.00B/s,0.00B/s
2022-03-16 10:59:47     12      20      -1      -1      -1      -1      -1      ens3:60,134     0.00B/s,0.00B/s
2022-03-16 10:59:52     14      15      -1      -1      -1      -1      -1      ens3:390,204    0.00B/s,0.00B/s
2022-03-16 10:59:56     13      21      -1      -1      -1      -1      -1      ens3:870,1018   0.00B/s,64.53K/s
2022-03-16 11:00:01     13      21      -1      -1      -1      -1      -1      ens3:162,192    0.00B/s,0.00B/s
2022-03-16 11:00:06     13      21      -1      -1      -1      -1      -1      ens3:648,546    0.00B/s,72.62K/s
2022-03-16 11:00:10     14      21      -1      -1      -1      -1      -1      ens3:648,498    0.00B/s,0.00B/s
```

stress_info记录了stress加压的相关信息，包括stress的开始时间、结束时间、测试类型、压力大小以及执行的命令
```bash
$ cat /tmp/collect/stress_info

begin-timestamp end-timestamp   type    stress  command
2022-03-16 10:53:52     2022-03-16 10:54:22     cpu     1       stress-ng --quiet --timeout 30s --cpu 1
2022-03-16 10:54:27     2022-03-16 10:54:57     cpu     2       stress-ng --quiet --timeout 30s --cpu 2
2022-03-16 10:55:02     2022-03-16 10:55:32     cpu     3       stress-ng --quiet --timeout 30s --cpu 3
2022-03-16 10:55:37     2022-03-16 10:56:07     cpu     4       stress-ng --quiet --timeout 30s --cpu 4
2022-03-16 10:56:12     2022-03-16 10:56:42     cpu     5       stress-ng --quiet --timeout 30s --cpu 5
```

stress_usage记录了stress进程占用的资源，包括采集时间戳、cpu使用(100 = 1 core)、内存使用率、disk IO
```bash
$ cat /tmp/collect/stress_usage

2022-03-16 11:05:02     0       0       0.00B/s 0.00B/s
2022-03-16 11:05:03     0       3.7     0.00B/s 0.00B/s
2022-03-16 11:05:05     81      14.4    0.00B/s 0.00B/s
2022-03-16 11:05:06     93.3    26      0.00B/s 0.00B/s
2022-03-16 11:05:07     100     26.3    0.00B/s 0.00B/s
2022-03-16 11:05:08     103     26.3    0.00B/s 0.00B/s
2022-03-16 11:05:09     106     26.3    0.00B/s 0.00B/s
2022-03-16 11:05:10     94.1    26.3    0.00B/s 0.00B/s
2022-03-16 11:05:12     96.8    26.3    0.00B/s 0.00B/s
2022-03-16 11:05:13     99      26.3    0.00B/s 0.00B/s
2022-03-16 11:05:14     100     26.3    0.00B/s 0.00B/s
2022-03-16 11:05:15     102     26.3    0.00B/s 0.00B/s
2022-03-16 11:05:16     103     26.3    0.00B/s 0.00B/s
2022-03-16 11:05:18     97.4    5.8     0.00B/s 0.00B/s
```



