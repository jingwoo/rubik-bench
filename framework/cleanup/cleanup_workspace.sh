#!/usr/bin/env bash

function clean_workspace() {
  local test_lock=${1}
  rm -rf "$@"
  # rm -rf native_solution_bench.yaml
  # rm -rf ./logs > /dev/null
  echo "Cleaning up workspace"
}

clean_workspace "$@"
