#!/usr/bin/env bash

function generate_build_common_config() {
    local taskID=${1}
    local config=${2}
  echo -e "\
config:
  log-path: ./logs/native_solution_bench-build-${taskID}.log
  show-failure-report: false
  show-summary-errors: true
  show-task-output: true
  collapse-on-completion: true
  stop-on-failure: true
  success-status-color: 76
  running-status-color: 190
  pending-status-color: 237
  error-status-color: 160" > ${config}
}

function generate_component_build_tasks() {
    
}

function execute_component_build_task() {
  echo ""
  echo -e "Kubernetes component build..."
  bashful run ${custom_solution_bench_config}
}

function generate_component_build_bashful_config() {
    generate_build_common_config ${1} ${2}
    generate_component_build_tasks

}

function build_k8s() {
  generate_component_build_bashful_config "$@"
  execute_component_build_task
}

build_k8s "$@"
